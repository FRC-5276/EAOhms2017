package org.usfirst.frc.team5276.robot.utilities;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.SpeedController;

public class PIDSpeedController implements SpeedController {
	PIDController pidController;
	double inputRange;
	SpeedController output;
	/**
	 * This creates a speed controller that implements PID in itself. 
	 * @param Kp Proportional Constant
	 * @param Ki Integral Constant
	 * @param Kd Differential Constant
	 * @param source PIDSource
	 * @param output PIDOutput
	 * @param inputRange This is the maximum and minimum value of the PIDSource. 
	 */
	public PIDSpeedController(double Kp, double Ki, double Kd, PIDSource source, SpeedController output, double inputRange) {
		// TODO Auto-generated constructor stub
		pidController = new PIDController(Kp, Ki, Kd, source, output);
		pidController.setOutputRange(-1, 1);
		this.inputRange = inputRange;
		this.output = output;
	}
	
	@Override
	public void pidWrite(double output) {
		set(output);
	}

	@Override
	public double get() {
		return pidController.getSetpoint()/inputRange;
	}

	@Override
	public void set(double speed) {
		pidController.setSetpoint(speed*inputRange);
	}

	@Override
	public void setInverted(boolean isInverted) {
		output.setInverted(isInverted);
		
	}

	@Override
	public boolean getInverted() {
		return output.getInverted();
	}

	@Override
	public void disable() {
		output.disable();
	}

	@Override
	public void stopMotor() {
		output.stopMotor();
	}

}
