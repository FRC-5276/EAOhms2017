package org.usfirst.frc.team5276.robot.subsystems;

import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.usfirst.frc.team5276.robot.GripPipeline;
import org.usfirst.frc.team5276.robot.Robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.vision.VisionThread;

/**
 *
 */
public class VisionSubsystem extends Subsystem {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	private VisionThread visionThread;
	private double centerX1 = 0.0;
	private double centerY1 = 0.0;
	private double centerX2 = 0.0;
	private double centerY2 = 0.0;
	private final Object imgLock = new Object();
	private final UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
	
	public VisionSubsystem() {
//		Robot.visionSubsystem.camera.setResolution(640, 480);
//		Robot.visionSubsystem.camera.setBrightness(0);
		visionThread = new VisionThread(camera, new GripPipeline(), pipeline -> {
	        if (!pipeline.filterContoursOutput().isEmpty()) {
	            Rect r1 = Imgproc.boundingRect(pipeline.filterContoursOutput().get(0));
	            Rect r2 = Imgproc.boundingRect(pipeline.filterContoursOutput().get(1));
	            synchronized (imgLock) {
	                centerX1 = (r1.x + (r1.width / 2));
	                centerY1 = r1.y + (r1.height / 2);
	                centerX2 = r2.x + (r2.width / 2);
	                centerY2 = r2.y + (r2.height / 2);
	            };
	        }
	        else {
	        	synchronized(imgLock) {
	        		centerX1 = 0;
	        		centerX2 = 0;
	        		centerY1 = 0;
	        		centerY2 = 0;
	        	}
	        }
	    });
	    visionThread.start();
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
   
    public void stop() {
    	camera.free();
    	//TODO: find a way to safely stop vision thread and camera feed
    }

	public double getCenterX1() {
		synchronized (imgLock){
			return centerX1;
		}
	}
	public double getCenterX2() {
		synchronized (imgLock){
			return centerX1;
		}
	}
	public double getCenterY1() {
		synchronized (imgLock){
			return centerX1;
		}
	}
	public double getCenterY2() {
		synchronized (imgLock){
			return centerX1;
		}
	}
}

